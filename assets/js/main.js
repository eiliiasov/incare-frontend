function onMessageReceived() {
	// Пролистываем список сообщений к концу
	$('#page-chat-body-body-scroller').scrollTop( $('#page-chat-body-body-scroller .content').outerHeight());
	
	// Для картинок добавляем обработчик
	$('.attached-image .page-chat-message-attachment').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
		image: {
			verticalFit: true
		}
	});
	
	$('.attached-video .page-chat-message-attachment').magnificPopup({
		type: 'iframe',
	    mainClass: 'mfp-fade',
	    removalDelay: 160,
	    preloader: false,
	    fixedContentPos: false,
	    iframe: {
	        markup: '<div class="mfp-iframe-scaler">'+
	                '<div class="mfp-close"></div>'+
	                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
	              '</div>',
	
	        srcAction: 'iframe_src',
	        }
	});
	
	$('.attached-audio .page-chat-message-attachment-audio').magnificPopup({
		type: 'iframe',
	    mainClass: 'mfp-fade',
	    removalDelay: 160,
	    preloader: false,
	    fixedContentPos: false,
	    iframe: {
	        markup: '<div class="mfp-iframe-scaler">'+
	                '<div class="mfp-close"></div>'+
	                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
	              '</div>',
	
	        srcAction: 'iframe_src',
	        }
	});
}
	
	
	

(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.get(0).clientHeight;
    }
})(jQuery);

$(document).ready(function() {
	
	setTimeout(function() {
		$('body').addClass('active');
		setTimeout(function() {
			$('body').addClass('scrollable-x');
		}, 800);
	}, 800);
	
	$('#page-chat-list-scroller,#page-chat-body-body-scroller, #page-chat-userinfo-scroller').perfectScrollbar();
	$('#page-contacts-list-scroller, #page-contacts-body-body-scroller, #page-contacts-userinfo-scroller, #modal-chat-history-scroller').perfectScrollbar();
	
	$('.ps-scroller').perfectScrollbar();
	
	

	$('#page-chat-body-textarea-textarea').keyup(function() {
		if ( $(this).val().length === 0 ) {
			$(this).addClass('empty');
		} else {
			$(this).removeClass('empty');
		}
		
		if ( $(this).hasScrollBar() ) {
			$(this).parent().addClass('scrollbar');
		} else {
			$(this).parent().removeClass('scrollbar');
		}
	});
	
	// ScrollDown Chat Body
	// With Animation
	// $('#page-chat-body-body-scroller').animate({scrollTop: $('#page-chat-body-body-scroller').height() }, 300);
	
	
	
	
	// Chat select
	$('.page-chat-list-item').click(function() {
		$('.page-chat-list-item').removeClass('active');
		$(this).addClass('active');
	});
	
	$(window).resize(function() {
		$('#operator').css('width', $('#page-chat-userinfo,#page-contacts-userinfo').outerWidth());
	});
	
	
	onMessageReceived();
	
	$(window).resize();
	
	
		
	// Dropdown
	$(document).mouseup(function (e) {
	    var container = $(".dropdown-menu,.dropdown");
		if (!container.is(e.target) // if the target of the click isn't the container...
	        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	        container.removeClass('active').removeClass('open');
	    }
	});
	
	$('.dropdown-menu a').click(function() {
		$('.dropdown-menu').removeClass('open');
		$('.dropdown').removeClass('active');
		return false;
	});
	
	$('.dropdown').click(function() {
		$('.dropdown-menu:not('+$(this).attr('data-dropdown')+')').removeClass('open');
		$('.dropdown').not($(this)).removeClass('active');
		
		$(this).toggleClass('active');
		$( $(this).attr('data-dropdown') ).toggleClass('open');
	});
		
		
	// #fn-set-online
	$('#fn-set-online').click(function() {
		$('#operator').removeClass('status-away');
		$('#operator').addClass('status-online');
	});
	$('#fn-set-away').click(function() {
		$('#operator').removeClass('status-online');
		$('#operator').addClass('status-away');
	});
	
	// #dropdown-quickresponses
	$('#dropdown-quickresponses li a').click(function() {
		$('#page-chat-body-textarea-textarea').val( $(this).text() );
		$('#page-chat-body-textarea-textarea').keyup(); // Чтобы обновить состояние
	});
	
	
	
	// Modal

	
	$('.modal-close').click(function() {
		$.magnificPopup.close();
	});
	
	$('#button-close-chat').magnificPopup({
	  items: {
	      src: '#modal-confirmation',
	      type: 'inline'
	  },
	  removalDelay: 130,
	  mainClass: 'mfp-fade'
	});
	
	
	// Modal for Contacts
	$('#button-delete-contact').magnificPopup({
	  items: {
	      src: '#modal-confirmation',
	      type: 'inline'
	  },
	  removalDelay: 130,
	  mainClass: 'mfp-fade'
	});
	
	$('#button-add-contact,#page-contacts-list-add-contact').magnificPopup({
	  items: {
	      src: '#modal-add-contact',
	      type: 'inline'
	  },
	  removalDelay: 130,
	  mainClass: 'mfp-fade'
	});
	
	$('#button-open-chat').magnificPopup({
	  items: {
	      src: '#modal-chat-history',
	      type: 'inline'
	  },
	  removalDelay: 130,
	  mainClass: 'mfp-fade'
	});
	
	
	
	$('#button-forward-to-operator').magnificPopup({
	  items: {
	      src: '#modal-chat-forward-to-operator',
	      type: 'inline'
	  },
	  removalDelay: 130,
	  mainClass: 'mfp-fade'
	});
	
	$('#button-forward-to-group').magnificPopup({
	  items: {
	      src: '#modal-chat-forward-to-group',
	      type: 'inline'
	  },
	  removalDelay: 130,
	  mainClass: 'mfp-fade'
	});
	
	$('#button-add-specialist').magnificPopup({
	  items: {
	      src: '#modal-chat-add-specialist',
	      type: 'inline'
	  },
	  removalDelay: 130,
	  mainClass: 'mfp-fade'
	});
	
	$('#button-call-supervisor').magnificPopup({
	  items: {
	      src: '#modal-chat-call-supervisor',
	      type: 'inline'
	  },
	  removalDelay: 130,
	  mainClass: 'mfp-fade'
	});
	
	
	
	$('[data-modal]').click(function() {
		var modalid = $(this).attr('data-modal');
		
		$.magnificPopup.open({
		  items: {
		      src: modalid,
		      type: 'inline'
		  },
		  removalDelay: 130,
		  mainClass: 'mfp-fade'
		});
		
		return false;
	});
	
	
	// Datepicker
	if ( $.datetimepicker ) {
		$.datetimepicker.setLocale('ru');
		
		$('.datetimepicker').each(function() {
			var pickerformat = $(this).attr('data-format');
			$(this).datetimepicker({
				format: pickerformat
			});
		});
		

		$('.timepicker').each(function() {
			var pickerformat = $(this).attr('data-format');
			
			$(this).datetimepicker({
				format: pickerformat,
				datepicker:false,
				step: 15
			});
		});	
			
	}
	
	
	
	// page-scroller
	$(window).resize(function() {
		$('.page-scroller').css('max-height', $(window).outerHeight() - $('.page-navbar').outerHeight() );
		$('.page-content').css('min-height', $(window).outerHeight() - $('.page-navbar').outerHeight() );
	});
	$(window).resize();
	
	
	// Multipleselect
	$("select.multipleselect").multipleSelect({
        placeholder: $(this).attr('data-placeholder'),
        selectAll: false
    });
	
	
	// Gallery
	$('.gallery-item').click(function() {
		var hadClass = $(this).hasClass('selected');
		$('.gallery-item').removeClass('selected');
		
		// Включаем или выключаем кнопки
		$('#btn-gallery-delete, #btn-gallery-forward').prop('disabled', true);
		
		if ( ! hadClass ) {
			$(this).addClass('selected');
			$('#btn-gallery-delete, #btn-gallery-forward').prop('disabled', false);
		}
		
	});
	
	$('.gallery-item.gallery-item-image').dblclick(function() {
		$.magnificPopup.open({
		  items: {
		    src: $(this).attr('data-href')
		  },
		  type: 'image'
		}, 0);
	});
	$('.gallery-item.gallery-item-video').dblclick(function() {
		$.magnificPopup.open({
			items: {
			    src: $(this).attr('data-href')
			  },
			type: 'iframe',
		    mainClass: 'mfp-fade',
		    removalDelay: 160,
		    preloader: false,
		    fixedContentPos: false,
		    iframe: {
	        markup: '<div class="mfp-iframe-scaler">'+
	                '<div class="mfp-close"></div>'+
	                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
	              '</div>',
	
	        srcAction: 'iframe_src',
	        }
        },0);
	});
	$('.gallery-item.gallery-item-audio').dblclick(function() {
		$.magnificPopup.open({
			items: {
			    src: $(this).attr('data-href')
			  },
			type: 'iframe',
		    mainClass: 'mfp-fade',
		    removalDelay: 160,
		    preloader: false,
		    fixedContentPos: false,
		    iframe: {
	        markup: '<div class="mfp-iframe-scaler">'+
	                '<div class="mfp-close"></div>'+
	                '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
	              '</div>',
	
	        srcAction: 'iframe_src',
	        }
        },0);
	});
	
	
	
	
	// Horizontal Menu Scroller
	function resetPnsButtons() {
		if ( $('.page-navbar-scroll').css('margin-left') == 0 ) {
			
		}
	}
	
	// Max min-w
	
	$('.page-navbar-scroll .pns-l').click(function() {
		if ( parseInt($('.page-navbar-scroll .page-navbar-tabs').css('margin-left')) < 0  )
			$('.page-navbar-scroll .page-navbar-tabs').css({marginLeft: '+=80px'});
			
		if ( parseInt($('.page-navbar-scroll .page-navbar-tabs').css('margin-left')) < 49  )
			$('.page-navbar-scroll .page-navbar-tabs').css({marginLeft: '0px'});
	});
	$('.page-navbar-scroll .pns-r').click(function() {
		
		var UlWidth = 0;
		$('.page-navbar-tabs ul li').each(function() {
			UlWidth = parseInt( $(this).outerWidth() ) + UlWidth;
		});
		
		var maxMinW = UlWidth - parseInt($('.page-navbar-scroll').outerWidth());
		if ( parseInt($('.page-navbar-scroll .page-navbar-tabs').css('margin-left')) >= -1 * maxMinW - 59 )
			$('.page-navbar-scroll .page-navbar-tabs').css({marginLeft: '-=80px'});
	});
	

	
});
	


